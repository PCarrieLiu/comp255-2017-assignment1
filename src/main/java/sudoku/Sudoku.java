
package org.bitbucket.sudoku;

/**
 * Sudoku
 */
public class Sudoku {

  /**
   * Constructor for Sudoku
   *
   * @param g   The grid that defines the sudoku
   */
  public Sudoku(int[][] g) {
    theGrid = g;
  }

  /**
   * The n x m grid that defines the Sudoku
   */
  private int[][] theGrid;

  /**
   *  Check validity of a Sudoku grid
   *
   *  @return true if and only if theGrid is a valid Sudoku
   */
  public boolean isValid()  {
    //  check size of grid
    if (theGrid.length == 0)
      return true;
    else {
      //  Grid is not empty
      // FIXME
      return false;
    }
  }
}
